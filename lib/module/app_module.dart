import 'package:siswaapp/utils/import_all.dart';


class AppModule extends Module{

  @override
  List<Bind> get binds => [];

  @override
  List<ModularRoute> get routes => [
    ChildRoute('/splash-screen/', child: (context, args)    =>  const SplashScreen()),
    ModuleRoute("/account-module/", module: AccountModule()),
    ModuleRoute("/siswa-module/",   module: SiswaModule(),guards: [AuthGuard()]),
  ];
}