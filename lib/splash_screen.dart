import 'package:siswaapp/utils/import_all.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Timer(const Duration(milliseconds: 100),() async {
      if(mounted){
        startSplashScreen();
      }
    });


  }
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Container(
          color: Colors.white,
          child: Column(
            mainAxisAlignment:MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset("assets/images/img_splash.png",),
              Text(
                "Siswa App",
                textAlign: TextAlign.center,
                style: StylePrimary.style70020colorPrimary
                    .copyWith(color:HexColor(HexColor.colorPrimary)),
              ),
            ],
          ),
        ),
      ),
    );
  }

  startSplashScreen() async {
    var duration = const Duration(seconds: 1);
    return Timer(duration,() async {
      Modular.to.navigate('/account-module/login-screen/');
    });
  }
}
