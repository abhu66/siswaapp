
import 'package:siswaapp/utils/import_all.dart';


class AccountModule extends Module{

  @override
  List<Bind> get binds => [
    Bind.factory((i) => AccountController()),
  ];

  @override
  List<ModularRoute> get routes => [
    ChildRoute("/login-screen/", child: (context, args) =>  const LoginScreen(),transition: TransitionType.downToUp),

  ];
}