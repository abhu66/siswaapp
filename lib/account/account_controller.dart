import 'dart:developer';

import '../utils/import_all.dart';

class AccountController extends ChangeNotifier {
  bool _isLoading      = false;
  bool _isObSecureText = false;
  TextEditingController _userNameText  = TextEditingController();
  TextEditingController _password = TextEditingController();

  void doLogin({required BuildContext context}){
    _isLoading = true;
    notifyListeners();
    Future.delayed(const Duration(seconds: 5)).then((value){
       if(_userNameText.value.text == "admin" && _password.value.text == "admin123"){
         _isLoading = false;
         Modular.to.navigate("/siswa-module/siswa_screen/");
       }
       else {
         _isLoading = false;
         displayErrorMotionToast(context: context,title: "Login Gagal",errorMessage: "Username atau Password salah !");
       }

      notifyListeners();
    });
  }



  //-----------setter getter-----------
  bool get isLoading => _isLoading;
  set isLoading(bool value) {
    _isLoading = value;
  }

  bool get isObSecureText => _isObSecureText;
  set isObSecureText(bool value) {
    _isObSecureText = value;
  }

  TextEditingController get password => _password;
  set password(TextEditingController value) {
    _password = value;
  }

  TextEditingController get userNameText => _userNameText;
  set userNameText(TextEditingController value) {
    _userNameText = value;
  }


}