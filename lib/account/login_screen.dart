import 'package:siswaapp/utils/import_all.dart';


class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:HexColor(HexColor.colorBackground),
      body:  ChangeNotifierProvider(
          create:(context) => AccountController(),
          child: Consumer<AccountController> (
              builder: (context, provider, child) {
                return ListView(
                    children: [
                      Image.asset("assets/images/img_logo_login.png",height: 300,width: 300,),
                      Container(
                        height: MediaQuery.of(context).size.height,
                        decoration:  BoxDecoration(
                            color: Colors.white,
                            borderRadius: const BorderRadius.only(
                                topRight: Radius.circular(30),
                                topLeft: Radius.circular(30)
                            ),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  blurRadius: 10,
                                  offset: const Offset(4,0),
                                  spreadRadius: 1
                              )
                            ]
                        ),
                        child: ListView(
                          padding: const EdgeInsets.only(left: 20,right: 20,bottom:
                          20,top: 20),
                          shrinkWrap:true,
                          physics: const NeverScrollableScrollPhysics(),
                          children: [
                            Text("Log-in",style:StylePrimary.style70020colorPrimary,),
                            const SizedBox(height: 20,),
                            Padding(
                              padding: const EdgeInsets.only(left: 20,right: 20),
                              child: widgetTextFormField(controller: provider.userNameText),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20,right: 20),
                              child: widgetTextFormPasswordField(controller: provider.password,provider: provider),
                            ),
                            const SizedBox(height: 10,),
                            Padding(
                              padding: const EdgeInsets.only(left: 20,right: 20),
                              child: Text("Forget password?",
                                textAlign: TextAlign.end,
                                style:StylePrimary.style40012colorPrimary,),
                            ),
                            const SizedBox(height: 20,),
                            InkWell(
                              onTap:provider.isLoading ? null : (){
                                setState(() {
                                  provider.doLogin(context: context);
                                });
                              },
                              child: Container(
                                margin: const EdgeInsets.only(left: 20,right: 20),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30),
                                  color:HexColor(HexColor.colorPrimary),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child:  provider.isLoading ? const Center(
                                    child: SizedBox(
                                        height: 24,
                                        width: 24,
                                        child: CircularProgressIndicator(color: Colors.white,strokeWidth: 4,)),
                                  ) : Text("Login",
                                    textAlign: TextAlign.center,
                                    style: StylePrimary.style60014colorPrimary.copyWith(color: Colors.white),),
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  );
              }
          )
      )
    );
  }

  Widget widgetTextFormField({required TextEditingController controller}){
    return TextFormField(
      style: StylePrimary.style60012colorPrimary,
      controller: controller,
      decoration:  InputDecoration(
        hintText: 'Input nim',
        labelText: 'NIM',

        hintStyle: StylePrimary.style40012colorPrimary.copyWith(color: HexColor(HexColor.colorGreyText)),
        labelStyle: StylePrimary.style40012colorPrimary,
        enabledBorder:  UnderlineInputBorder(
            borderSide: BorderSide(color:HexColor(HexColor.colorPrimary),
          ),
        ),
        focusedBorder:  UnderlineInputBorder(
          borderSide: BorderSide(color:HexColor(HexColor.colorPrimary),
          ),
        ),

    ),
      onSaved: (String? value) {
        // This optional block of code can be used to run
        // code when the user saves the form.
      },
      validator: (String? value) {
        return (value != null && value.contains('@')) ? 'Do not use the @ char.' : null;
      },
    );
  }
  Widget widgetTextFormPasswordField({required TextEditingController controller,required AccountController provider}){
    return TextFormField(
      controller: controller,
      style: StylePrimary.style60012colorPrimary,
      obscureText: !provider.isObSecureText,
      decoration:  InputDecoration(
        hintText: 'Input password',
        labelText: 'Password',
        hintStyle: StylePrimary.style40012colorPrimary.copyWith(color: HexColor(HexColor.colorGreyText)),
        labelStyle: StylePrimary.style40012colorPrimary,
        suffix:
    InkWell(
        onTap: (){
          setState(() {
            provider.isObSecureText = !provider.isObSecureText;
          });
        },
        child: !provider.isObSecureText ? Icon(Icons.remove_red_eye_rounded,color: HexColor(HexColor.colorPrimary)) :
    Icon(Icons.visibility_off_rounded,color: HexColor(HexColor.colorPrimary),)),
        enabledBorder:  UnderlineInputBorder(
            borderSide: BorderSide(color:HexColor(HexColor.colorPrimary),
          ),
        ),
        focusedBorder:  UnderlineInputBorder(
          borderSide: BorderSide(color:HexColor(HexColor.colorPrimary),
          ),
        ),

    ),
      onSaved: (String? value) {
        // This optional block of code can be used to run
        // code when the user saves the form.
      },
      validator: (String? value) {
        return (value != null && value.contains('@')) ? 'Do not use the @ char.' : null;
      },
    );
  }
}
