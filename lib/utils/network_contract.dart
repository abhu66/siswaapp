abstract class NetworkContract{

  void onLoading(bool onLoading);
  void onSuccess(dynamic onSuccess);
  void onError(dynamic onError);

}