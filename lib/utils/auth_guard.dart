import 'dart:async';
import 'dart:developer';

import 'package:intl/intl.dart';

import 'import_all.dart';

class AuthGuard extends RouteGuard {
  AuthGuard() : super(redirectTo: '/login-module/login-screen/');

  @override
  FutureOr<bool> canActivate(String path, ParallelRoute route) async {
    return true;
  }
}