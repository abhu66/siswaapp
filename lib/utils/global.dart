import 'package:motion_toast/resources/arrays.dart';

import 'import_all.dart';

void displayErrorMotionToast({required BuildContext context,dynamic title,dynamic errorMessage}) {
  final snackBar = SnackBar(
    content: Text('$title \n$errorMessage',style: StylePrimary.style60012colorPrimary.copyWith(color: Colors.white),),
    backgroundColor: Colors.red,
    action: SnackBarAction(
      label: 'Dismiss',
      textColor: Colors.white,
      onPressed: () {
      },
    ),
  );
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

void displaySuccessMotionToast({required BuildContext context,dynamic title,dynamic succssMessage}) {
  final snackBar = SnackBar(
    content: Text('$title \n$succssMessage',style: StylePrimary.style60012colorPrimary.copyWith(color: Colors.white),),
    backgroundColor: Colors.red,
    action: SnackBarAction(
      label: 'Dismiss',
      textColor: Colors.white,
      onPressed: () {
      },
    ),
  );
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}