import 'dart:developer';

import 'package:siswaapp/main.dart';
import 'package:sqflite/sqflite.dart';

class TblSiswa {
  static const tableSiswa   = 'tbl_siswa';
  static const uuid      = 'uuid';
  static const nim      = 'nim';
  static const name     = 'nama';
  static const alamat   = 'alamat';
  static const kelas    = 'kelas';

  static createTable(Database db, int version) async {
    await db.execute("DROP TABLE IF EXISTS $tableSiswa");
    await db.execute('''
          CREATE TABLE $tableSiswa (
            $uuid TEXT PRIMARY KEY NOT NULL,
            $nim TEXT NOT NULL UNIQUE,
            $name TEXT NOT NULL,
            $alamat TEXT NOT NULL,
            $kelas TEXT NOT NULL
          )
          ''').catchError((onError){
            log("error $onError");
    });
  }

  static Future<int> insert(Map<String, dynamic> row) async {
    Database db = await dbHelper.database;
    return await db.insert(tableSiswa, row);
  }
  static insertBatch(List data) async {
    Database db = await dbHelper.database;
    var batch   = db.batch();
    int success = 0;
    data.map((e){
      Map<String, dynamic> row = {
        TblSiswa.uuid       : e['uuid'],
        TblSiswa.nim        : e['nim'],
        TblSiswa.name       : e['nama'],
        TblSiswa.alamat     : e['alamat'],
        TblSiswa.kelas      : e['kelas'],

      };
      batch.insert(tableSiswa, row,conflictAlgorithm: ConflictAlgorithm.replace);
    }).toList();

    try {
      await batch.commit(continueOnError: false).then((value) async {
        success = 1;
      }).catchError((error){
        success = -1;
      });

    } on DatabaseException catch (e) {
      success = -1;
    }
    return success;
  }


  static Future<List<Map<String, dynamic>>> queryAllRecordsAll({dynamic nim,dynamic name}) async {
    Database db = await dbHelper.database;
    log("QUERY SEARCH SELECT * FROM $tableSiswa "
        "WHERE nim LIKE '%$nim%' "
        "AND nama LIKE '%$name%'");
    return await db.rawQuery("SELECT * FROM $tableSiswa "
        "WHERE nim LIKE '%$nim%' "
        "OR nama LIKE '%$name%' "
        "");
  }

}