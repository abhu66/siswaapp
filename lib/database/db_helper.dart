
import 'dart:developer';

import 'package:siswaapp/database/tbl_siswa.dart';
import 'package:siswaapp/utils/import_all.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DatabaseHelper {

  static const _databaseName    = "siswaapp_db";
  static const _databaseVersion = 1;

  // make this a singleton class
  DatabaseHelper._privateConstructor();

  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  // only have a single app-wide reference to the database
  static Database? _database;

  Future<Database> get database async {
    if (_database != null) return _database!;
    // lazily instantiate the db the first time it is accessed
    _database = await _initDatabase();
    return _database!;
  }

  // this opens the database (and creates it if it doesn't exist)
  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path,_databaseName);
    log("path database ===> $path");
    return await openDatabase(path,
        version: _databaseVersion,
        onCreate: _onCreate,
        onUpgrade: _onUpgrade
    );
  }

  // UPGRADE DATABASE TABLES
  Future _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      // you can execute drop table and create table
      TblSiswa.createTable(db, newVersion);
    }
  }

  // SQL code to create the database table
  Future _onCreate(Database db, int version) async {
    TblSiswa.createTable(db, version);
  }

  // Inserts a row in the database where each key in the Map is a column name
  // and the value is the column value. The return value is the id of the
  // inserted row.
  Future<int> insert(Map<String, dynamic> row,{dynamic tableName}) async {
    Database db = await instance.database;
    return await db.insert(tableName, row);
  }

  // All of the rows are returned as a list of maps, where each map is
  // a key-value list of columns.
  Future<List<Map<String, dynamic>>> queryAllRows({dynamic tableName}) async {
    Database db = await instance.database;
    return await db.query(tableName);
  }


  //Get all records which are unsynched
  Future<List<Map<String, dynamic>>> queryUnsynchedRecords({dynamic tableName}) async {
    Database db = await instance.database;
    return await db.rawQuery('SELECT * FROM $tableName WHERE status_online = 0');
  }

  Future<List<Map<String, dynamic>>> queryAllRecords({dynamic tableName}) async {
    Database db = await instance.database;
    return await db.rawQuery('SELECT * FROM $tableName');
  }


  // All of the methods (insert, query, update, delete) can also be done using
  // raw SQL commands. This method uses a raw query to give the row count.
  Future<int?> queryRowCount({dynamic tableName}) async {
    Database db = await instance.database;
    return Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM $tableName'));
  }

  // We are assuming here that the id column in the map is set. The other
  // column values will be used to update the row.
  Future<int> update(Map<String, dynamic> row,{dynamic tableName,dynamic columnId}) async {
    Database db = await instance.database;
    int id = row[columnId];
    return await db.update(tableName, row, where: '$columnId = ?', whereArgs: [id]);
  }

  // Deletes the row specified by the id. The number of affected rows is
  // returned. This should be 1 as long as the row exists.
  Future<int> delete(int id,{dynamic tableName,dynamic columnId}) async {
    Database db = await instance.database;
    return await db.delete(tableName, where: '$columnId = ?', whereArgs: [id]);
  }
}