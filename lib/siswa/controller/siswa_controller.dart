import 'dart:developer';

import 'package:siswaapp/database/tbl_siswa.dart';
import 'package:siswaapp/utils/import_all.dart';
import 'package:uuid/uuid.dart';


class SiswaController extends ChangeNotifier {
  Timer? _debounce;
  bool _isLoading                         = false;
  dynamic selectedKelas;
  List  _dataSiswa                        = [];
  late  TextEditingController _cariSiswa  = TextEditingController();
  late  TextEditingController _nimText    = TextEditingController();
  late  TextEditingController _nameText   = TextEditingController();
  late  TextEditingController _alamat     = TextEditingController();


  void doCreateSiswa({required BuildContext context}) {
    _isLoading = true;
    notifyListeners();
    log("<===============================================>");
    log("INSERT DATA SISWA");
    log("<===============================================>");
    log("NIM      =====> ${_nimText.value.text}");
    log("Nama     =====> ${_nameText.value.text}");
    log("Alamat   =====> ${_alamat.value.text}");
    log("Kelas    =====> $selectedKelas");
    log("<===============================================>");
    Map<String,dynamic> dataNewSiswa = {
      TblSiswa.uuid: const Uuid().v4().toString(),
      TblSiswa.nim:  _nimText.value.text,
      TblSiswa.name: _nameText.value.text,
      TblSiswa.alamat:_alamat.value.text,
      TblSiswa.kelas: selectedKelas,
    };
    TblSiswa.insert(dataNewSiswa).then((value){
      log("value $value");
      _isLoading = false;
      Navigator.of(context).pop();
      displaySuccessMotionToast(context: context,title: "Berhasil",succssMessage: "Tambah siswa berhasil!");
      notifyListeners();
    }).onError((error, stackTrace){
      Navigator.of(context).pop();
      if(error.toString().toLowerCase().contains("unique constraint")){
        displayErrorMotionToast(context: context,title: "Tambah Siswa Gagal",errorMessage: "Siswa dengan NIM ${_nimText.value.text} sudah terdaftar!");
      }
      else {
        displayErrorMotionToast(context: context,title: "Tambah Siswa Gagal",errorMessage: error.toString());
      }
      _isLoading = false;
      notifyListeners();
    });
  }


  Future<void> doGetListSiswa({required BuildContext context}) async {
    _isLoading = true;
    notifyListeners();
    log("<===============================================>");
    log("GET LIST DATA SISWA");
    log("<===============================================>");

    TblSiswa.queryAllRecordsAll(name: _cariSiswa.value.text,nim: _cariSiswa.value.text).then((value){
      Future.delayed(const Duration(seconds: 2)).then((ssss){
        _isLoading = false;
        _dataSiswa = value;
        notifyListeners();
        _dataSiswa.map((e){
          log("<===============================================>");
          log("NIM      =====> ${e['nim']}");
          log("Nama     =====> ${e['nama']}");
          log("Alamat   =====> ${e['alamat']}");
          log("Kelas    =====> ${e['kelas']}");
        }).toList();
        log("<===============================================>");
      });
    }).onError((error, stackTrace){
      displayErrorMotionToast(context: context,title: "Gagal",errorMessage: "Get data siswa gagal! ${error.toString()}");
      _isLoading = false;
      notifyListeners();
      log("<===============================================>");
    });


  }

  void onTextChanged({String? text,required BuildContext context}) {
    if (_debounce?.isActive ?? false) _debounce!.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      // This function will be called after a delay of 500 milliseconds
      Future.delayed(const Duration(seconds: 1)).then((value) async {
        doGetListSiswa(context: context);
      });
      // You can perform your search or other actions here
    });
  }


  //-----------setter getter-----------
  bool get isLoading => _isLoading;
  set isLoading(bool value) {
    _isLoading = value;
  }


  TextEditingController get nimText => _nimText;
  set nimText(TextEditingController value) {
    _nimText = value;
  }

  TextEditingController get nameText => _nameText;
  set nameText(TextEditingController value) {
    _nameText = value;
  }

  TextEditingController get alamat => _alamat;
  set alamat(TextEditingController value) {
    _alamat = value;
  }

  TextEditingController get cariSiswa => _cariSiswa;
  set cariSiswa(TextEditingController value) {
    _cariSiswa = value;
  }
  List get dataSiswa => _dataSiswa;
  set dataSiswa(List value) {
    _dataSiswa = value;
  }

}