
import 'package:siswaapp/utils/import_all.dart';


class SiswaModule extends Module{

  @override
  List<Bind> get binds => [
    Bind.factory((i) => AccountController()),
  ];

  @override
  List<ModularRoute> get routes => [
    ChildRoute("/siswa_screen/", child: (context, args) =>  const SiswaScreen(),transition: TransitionType.downToUp),

  ];
}