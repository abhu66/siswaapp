import 'package:siswaapp/utils/import_all.dart';


class SiswaScreen extends StatefulWidget {
  const SiswaScreen({Key? key}) : super(key:key);

  @override
  State<SiswaScreen> createState() => _SiswaScreenState();
}

class _SiswaScreenState extends State<SiswaScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => SiswaController()..doGetListSiswa(context: context),
      child: Consumer<SiswaController>(
        builder: (context, provider,child){

          return Scaffold(
            backgroundColor: HexColor("#F0FFFF"),
            appBar: AppBar(
              centerTitle: false,
              elevation: 0.0,
              iconTheme: const IconThemeData(color: Colors.white),
              backgroundColor: HexColor(HexColor.colorPrimary),
              title: Text(
                "Data Siswa",
                style: StylePrimary.style60016colorPrimary.copyWith(color: Colors.white),
              ),
              actions:  [
                InkWell(
                  onTap: (){
                    setState(() {
                      provider.doGetListSiswa(context: context);
                    });
                  },
                  child:const Padding(
                      padding: EdgeInsets.all(16.0),
                      child: Icon(Icons.refresh,color: Colors.white,)
                  ),
                ),
              ],
            ),
            body: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 16.0,right: 16.0,top: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(child: widgetSearchField(context: context,label: "Cari Data Siswa",provider: provider))
                    ],
                  ),
                ),
                Expanded(
                  child: provider.isLoading ? Skeletonizer(
                    enabled: provider.isLoading,
                    child: ListView.builder(
                      itemCount: 20,
                      itemBuilder: (context, index) {
                        return Card(
                          child: ListTile(
                            leading: Image.asset("assets/images/img_student.png"),
                            title: Text('Abu Khoerul IA',style: StylePrimary.style40014colorPrimary,),
                            subtitle: Text('Kelas $index',style: StylePrimary.style40014colorPrimary,),
                            trailing: const Icon(Icons.ac_unit),
                          ),
                        );
                      },
                    ),
                  ) :
                  ListView.builder(
                    itemCount: provider.dataSiswa.length,
                    itemBuilder: (context, index) {
                      var dataSiswa = provider.dataSiswa[index];
                      return Padding(
                        padding: const EdgeInsets.only(left: 16.0,right: 16.0,top: 5),
                        child: Card(
                          child: ListTile(
                            contentPadding: const EdgeInsets.all(10),
                            leading: Image.asset("assets/images/img_student.png"),
                            title: Text('${dataSiswa['nim']} - ${dataSiswa['nama']}',style: StylePrimary.style40014colorPrimary,),
                            subtitle: Text('Kelas ${dataSiswa['kelas']}\nAlamat : ${dataSiswa['alamat']}',style: StylePrimary.style40014colorPrimary,),
                            trailing: const Icon(Icons.ac_unit),
                          ),
                        ),
                      );
                    },
                  ),
                )
              ],
            ),
            floatingActionButton: FloatingActionButton(
              backgroundColor: HexColor(HexColor.colorPrimary),
              onPressed: (){
                setState(() {
                  showBottomSheetAddSiswa(context: context, provider: provider);
                });
              },
              child: const Icon(Icons.add,color: Colors.white,),
            ),
          );
        },
      )
    );
  }

  Widget widgetSearchField({required BuildContext context,required String label, required SiswaController provider}) {
    return Padding(
      padding: const EdgeInsets.only(right:0,bottom: 8,left:0.0,top: 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Flexible(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(
                    child: Container(
                      color: Colors.transparent,
                      width: MediaQuery.of(context).size.width,
                      child: TextFormField(
                          controller: provider.cariSiswa,
                          onChanged: (v) => provider.onTextChanged(context: context, text: v),
                          style:StylePrimary.style60016colorPrimary.copyWith(
                              color: HexColor(HexColor.color000C17),
                              fontSize: 14),
                          decoration: InputDecoration(
                              contentPadding: const EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 13),
                              fillColor: Colors.white,
                              filled: true,
                              prefixIcon:const Icon(Icons.search),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                  borderSide: BorderSide(
                                      color: Colors.grey.withOpacity(0.5), width: 1)),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                  borderSide: BorderSide(
                                      color: Colors.grey.withOpacity(0.5), width: 1)),
                              // focusedBorder: OutlineInputBorder(
                              //     borderRadius: BorderRadius.circular(10.0),
                              //     borderSide: BorderSide(
                              //         color: Colors.grey.withOpacity(0.5), width: 1)),
                              hintText: label,
                              hintStyle: StylePrimary.style60016colorPrimary.copyWith(
                                  color: HexColor(HexColor.color000C17)
                                      .withOpacity(0.45),
                                  fontSize: 14))),
                    ),
                  ),
                ],
              )),
        ],
      ),
    );
  }
  showBottomSheetAddSiswa({required BuildContext context,required SiswaController provider}){
    return showModalBottomSheet(
      context: context,
      elevation: 1.0,
      isScrollControlled: true,
      backgroundColor: Colors.white,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(30.0),
        ),
      ),
      builder: (BuildContext context) {
        return StatefulBuilder(
            builder: (context,setState) {
              return Padding(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: Container(
                  color: Colors.transparent,
                  child: Container(
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30.0),
                            topRight: Radius.circular(30.0))
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        ListTile(
                          title: Text('TAMBAH SISWA',style: StylePrimary.style70016colorPrimary.copyWith(color: Colors.black),),
                          trailing: InkWell(
                              onTap: (){
                                Navigator.of(context).pop();
                              },
                              child: Icon(Icons.clear,size:30,color: HexColor(HexColor.colorPrimary),)),
                        ),
                        const Divider(),
                        ListView(
                          padding: const EdgeInsets.only(top: 0),
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          children: [

                            Padding(
                              padding: const EdgeInsets.only(left: 16,right: 16,bottom: 10),
                              child: Text("NIM",style: StylePrimary.style60014colorPrimary,),
                            ),
                            Padding(
                                padding: const EdgeInsets.only(left: 16,right: 10),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Flexible(
                                        child: widgetInputField(context: context,label: "NIM", controller:provider.nimText)),
                                  ],
                                )),

                            const SizedBox(height: 16,),
                            Padding(
                              padding: const EdgeInsets.only(left: 16,right: 16,bottom: 10),
                              child: Text("Nama Siswa",style: StylePrimary.style60014colorPrimary,),
                            ),
                            Padding(
                                padding: const EdgeInsets.only(left: 16,right: 10),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Flexible(
                                        child: widgetInputField(context: context,label: "Nama Siswa", controller:provider.nameText)),
                                  ],
                                )),

                            const SizedBox(height: 16,),
                            Padding(
                              padding: const EdgeInsets.only(left: 16,right: 16,bottom: 10),
                              child: Text("Alamat",style: StylePrimary.style60014colorPrimary,),
                            ),
                            Padding(
                                padding: const EdgeInsets.only(left: 16,right: 10),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Flexible(
                                        child: widgetInputField(context: context,label: "Alamat", controller:provider.alamat)),
                                  ],
                                )),

                            const SizedBox(height: 16,),
                            Padding(
                              padding: const EdgeInsets.only(left: 16,right: 16,bottom: 10),
                              child: Text("Kelas",style: StylePrimary.style60014colorPrimary,),
                            ),
                            Padding(
                                padding: const EdgeInsets.only(left: 16,right: 16),
                                child: widgetDropDownKelas(label: "Pilih Kelas", context: context,provider: provider)),


                          ],
                        ),

                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: SizedBox(
                            height: 50,
                            width: MediaQuery.of(context).size.width,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(20.0), // Adjust the radius as needed
                              child: ElevatedButton(
                                style:ElevatedButton.styleFrom(
                                    backgroundColor: HexColor(HexColor.colorPrimary)
                                ) ,
                                onPressed: provider.isLoading ? null :  () {
                                  setState((){
                                    provider.doCreateSiswa(context: context);

                                  });
                                },
                                child: provider.isLoading ? const Center(
                                  child: SizedBox(
                                      height: 24,
                                      width: 24,
                                      child: CircularProgressIndicator(color: Colors.white,strokeWidth: 4,)),
                                ) : Text('Simpan',style: StylePrimary.style60014colorPrimary.copyWith(color: Colors.white),),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              );
            }
        );
      },
    ).then((value){
      setState((){
        provider.doGetListSiswa(context: context);
      });
    });
  }
  Widget widgetDropDownKelas({required BuildContext context,required String label,required SiswaController provider}) {
    return Padding(
      padding: const EdgeInsets.only(left:0,bottom: 8,right:0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Flexible(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: const BoxDecoration(
                      ),
                      child: DropdownButtonHideUnderline(
                        child: StatefulBuilder(
                            builder: (context,setState) {
                              return DropdownButton2<dynamic>(
                                isExpanded: true,
                                hint: Text(
                                    'Pilih Kelas',
                                    style: StylePrimary.style60016colorPrimary.copyWith(
                                        color: HexColor(HexColor.color000C17)
                                            .withOpacity(0.45),
                                        fontSize: 14)
                                ),
                                items:["1","2","3","4","5","6"].map((dynamic item) => DropdownMenuItem<dynamic>(
                                  value: item,
                                  child: Text(
                                      "Kelas $item ",
                                      style: StylePrimary.style40014colorPrimary
                                  ),
                                )).toList(),
                                value: provider.selectedKelas,
                                onChanged: (dynamic? value) {
                                  setState(() {
                                    provider.selectedKelas = value;
                                  });
                                },

                                buttonStyleData:  ButtonStyleData(
                                  padding: const EdgeInsets.symmetric(horizontal: 16),
                                  height: 45,
                                  width: 140,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    border: Border.all(
                                      width: 1,
                                      color: Colors.grey.withOpacity(0.5),
                                    ),
                                  ),
                                ),
                                menuItemStyleData: const MenuItemStyleData(
                                  height: 45,
                                ),
                              );
                            }
                        ),
                      ),
                    ),
                  ),
                ],
              )),
        ],
      ),
    );
  }
  Widget widgetInputField({required BuildContext context,required String label, required TextEditingController controller}) {
    return Padding(
      padding: const EdgeInsets.only(right:0,bottom: 8,left:0.0,top: 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Flexible(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(
                    child: Container(
                      color: Colors.transparent,
                      width: MediaQuery.of(context).size.width > 1000 ? 380 : MediaQuery.of(context).size.width,
                      child: TextFormField(
                          controller: controller,
                          style: StylePrimary.style40014colorPrimary,
                          decoration: InputDecoration(
                              contentPadding: const EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 13),
                              fillColor: Colors.white,
                              filled: true,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: BorderSide(
                                      color: Colors.grey.withOpacity(0.5), width: 1)),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: BorderSide(
                                      color: Colors.grey.withOpacity(0.5), width: 1)),
                              hintText: label,
                              hintStyle: StylePrimary.style60016colorPrimary.copyWith(
                                  color: HexColor(HexColor.color000C17)
                                      .withOpacity(0.45),
                                  fontSize: 14))
                      ),
                    ),
                  ),
                ],
              )),
        ],
      ),
    );
  }
}
