
import 'package:flutter/material.dart';


class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    int colors = 0;
    hexColor = hexColor.replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF$hexColor";
    }
    if (hexColor.length == 8) {
      colors = int.parse("0x$hexColor");
    }
    return colors;
  }
  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
  static String color62228     = "#e62228";
  static String colorb583b     = "#eb583b";
  static String colore83b31    = "#e83b31";
  static String colore7342f    = "#e7342f";
  static String colorPrimary   = "#072d64";
  static String colorSecondary = "#3A7CCE";
  static String colorBGSecondary = "#DCE7F2";
  static String colorGreyText  = "#909090";
  static String colorBlackText = "#041C26";
  static String color566789    = "#566789";
  static String colorE9E9E9    = "#E9E9E9";
  static String colorC4C4C4    = "#C4C4C4";
  static String color041C26    = "#041C26";
  static String colorfbce01    = "#fbce01";
  static String color2E3B51    = "#2E3B51";
  static String color060D17    = "#060D17";
  static String color77849D    = "#77849D";
  static String colorD8DDED    = "#D8DDED";
  static String colorDFDFDF    = "#DFDFDF";
  static String colorF7F9FF    = "#F7F9FF";
  static String color49359B    = "#49359B";
  static String color121518    = "#121518";
  static String colorFF0000    = "#FF0000";
  static String color4A4A4A    = "#4A4A4A";
  static String colorF5F5F5    = "#F5F5F5";
  static String color01AFAE    = "#01AFAE";
  static String colorFAFAFA    = "#FAFAFA";
  static String color04B2B1    = "#04B2B1";
  static String color1A1A1A    = "#1A1A1A";
  static String color909090    = "#909090";
  static String color3a3a3a    = "#3A3A3A";
  static String color3a3b45    = "#3a3b45";
  static String colorActiveIcon = "#0156a1";
  static String colorBgScreen  = "#eeeff0";
  static String color000C17    = "#000C17";
  static String color4d4d4d    = "#4D4D4D";
  static String colorE5E5E5   = "#E5E5E5";
  static String colorB3B3B3   = "#B3B3B3";
  static String colorF3F3F3   = "#F3F3F3";
  static String colorBackground   = "#f8fcfd";

  //radius
  static double radius8        = 8.0;
  static double radius10       = 10.0;
  static double radius12       = 12.0;
  static double radius55       = 55.0;
  static double padding16      = 16.0;
  static double padding24      = 24.0;

}
