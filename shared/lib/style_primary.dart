import 'shared.dart';


class StylePrimary {
  static TextStyle style30010colorPrimary = GoogleFonts.ubuntu(textStyle: TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 12.0,
      fontWeight: FontWeight.w300));
  static TextStyle style30012colorPrimary = GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 14.0,
      fontWeight: FontWeight.w300));
  static TextStyle style30014colorPrimary = GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 16.0,
      fontWeight: FontWeight.w300));
  static TextStyle style30016colorPrimary = GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 18.0,
      fontWeight: FontWeight.w300));
  static TextStyle style30018colorPrimary = GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 20.0,
      fontWeight: FontWeight.w300));
  static TextStyle style30020colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 22.0,
      fontWeight: FontWeight.w300));
  static TextStyle style30024colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 26.0,
      fontWeight: FontWeight.w300));

//400
  static TextStyle style40010colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 12.0,
      fontWeight: FontWeight.w400));
  static TextStyle style40012colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 14.0,
      fontWeight: FontWeight.w400));
  static TextStyle style40014colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 16.0,
      fontWeight: FontWeight.w400));
  static TextStyle style40016colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 18.0,
      fontWeight: FontWeight.w400));
  static TextStyle style40018colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 20.0,
      fontWeight: FontWeight.w400));
  static TextStyle style40020colorPrimary =  GoogleFonts.ubuntu(textStyle :  TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 22.0,
      fontWeight: FontWeight.w400));
  static TextStyle style40024colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 26.0,
      fontWeight: FontWeight.w400));

//600
  static TextStyle style60010colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 12.0,
      fontWeight: FontWeight.w600));
  static TextStyle style60012colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 14.0,
      fontWeight: FontWeight.w600));
  static TextStyle style60014colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 16.0,
      fontWeight: FontWeight.w600));
  static TextStyle style60016colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 18.0,
      fontWeight: FontWeight.w600));
  static TextStyle style60018colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 18.0,
      fontWeight: FontWeight.w600));
  static TextStyle style60020colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 22.0,
      fontWeight: FontWeight.w600));
  static TextStyle style60024colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 26.0,
      fontWeight: FontWeight.w600));

//700
  static TextStyle style70010colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 12.0,
      fontWeight: FontWeight.w700));
  static TextStyle style70012colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 14.0,
      fontWeight: FontWeight.w700));
  static TextStyle style70014colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 16.0,
      fontWeight: FontWeight.w700));
  static TextStyle style70016colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 18.0,
      fontWeight: FontWeight.w700));
  static TextStyle style70018colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 20.0,
      fontWeight: FontWeight.w700));
  static TextStyle style70020colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 22.0,
      fontWeight: FontWeight.w700));
  static TextStyle style70024colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 26.0,
      fontWeight: FontWeight.w700));
  static TextStyle style70028colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 30.0,
      fontWeight: FontWeight.w700));
  static TextStyle style60013colorPrimary =  GoogleFonts.ubuntu(textStyle : TextStyle(
      color: HexColor(HexColor.colorPrimary),
      fontSize: 12.0,
      fontWeight: FontWeight.w600));
}