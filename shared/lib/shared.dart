library shared;


export 'dart:convert';
export 'dart:io';

export 'package:device_info/device_info.dart';
export 'package:flutter/foundation.dart';
export 'package:flutter/material.dart';
export 'package:flutter/services.dart';
export 'package:font_awesome_flutter/font_awesome_flutter.dart';
export 'package:google_fonts/google_fonts.dart';
export 'package:location/location.dart';
export 'package:shared/function_shared.dart';
export 'package:shared/shared_colors.dart';
export 'package:shared/style_primary.dart';
export 'package:shared_preferences/shared_preferences.dart';
export 'package:url_launcher/url_launcher_string.dart';