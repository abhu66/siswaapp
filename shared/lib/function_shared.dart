import 'dart:developer';

import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import 'shared.dart';

class FunctionShared{
  static late Location location;
  static late Position locationData;
  static bool isGenerateRoutes = false;
  static bool enableRole       = false;
  static int  indexLocation    = 0;
  static bool  userMode        = true;

  static String baseUrlImage  = "assets/images/";
  static const userLoginKey   = "data_login";
  static const healthInfoKey  = "health_info_key";
  static const vehicleInfoKey = "vehicle_info_key";
  static const deviceInfoKey  = "device_info_key";
  static const p2hInfoKey     = "p2h_info_key";
  static const sitaInfoKey    = "vehicle_info_key";
  static const isLoggedInKey  = "is_logged_in_key";
  static bool activeMenu     = false;
  static const tokenKey      = "token_key";
  static String? token       = "";
  static Map<String, dynamic>? profile;
  static bool? healthInfo     = false;
  static bool? vehicleInfo    = false;
  static bool? p2hInfo        = false;
  static bool? sitaInfo       = false;
  static bool? isLoggedIn     = false;
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  static String deviceNameAndId = "";
  static String hari   = "";
  static bool? isAfterLogin   = false;
  static String afterLoginKey = "after_login_key";
  static String tableNameSync = "";
  static DateTime? lastSync;
  static bool isLoadingAll    = false;
  static String? isSyncAll;
  static DateTime? lastLogin;


  static String generateImage({required String fileName}){
    return baseUrlImage + fileName;
  }

  static saveToken(dynamic value,String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key,value.toString());

  }
  static saveDeviceId(dynamic value,String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key,value.toString());
    loadDeviceInfoNameIndId();
  }

  static loadToken() async {
    Future.delayed(Duration.zero).then((value) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String? tokens = prefs.getString(FunctionShared.tokenKey);
      token = tokens;
    });
  }

  static loadDay() async {
    Future.delayed(Duration.zero).then((value) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String? days = prefs.getString("hari");
      log("days $days");
      hari = days ?? "";
    });
  }

  static loadDeviceInfoNameIndId() async {
    Future.delayed(Duration.zero).then((value) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String? device = prefs.getString(FunctionShared.deviceInfoKey);
      deviceNameAndId = device ?? "";
    });
  }
  static saveObj(dynamic value,String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key,value.toString());
    setLoggedInApps(true);
    initDataUser();
  }
  static Future<dynamic> initDataUser() async {
    await Future.delayed(Duration.zero).then((value) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String? userPref = prefs.getString(userLoginKey);
      if(userPref != null) {
        FunctionShared.profile = json.decode(userPref);
      }
      log("data prof ${FunctionShared.profile}");
    });

    return FunctionShared.profile;
  }

  static saveBool(bool value,String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(key,value);
    prefs.reload();
  }

  static checkIsSyncAll() async {
    await Future.delayed(Duration.zero).then((value) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String? isR = prefs.getString("is_sync_all");
      isSyncAll = isR;
      log("isSyncAll $isSyncAll");
    });
  }

  static isLoadingApps() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    isLoadingAll = prefs.getBool("loading_all") ?? false;
    return isLoadingAll;
  }

  static Future<void> saveLastSynch(DateTime value,String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key,value.toString());
  }

  static Future<void> saveLastLogin(DateTime value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("last_login",value.toString());
  }

  static getLastSync() async {
    Future.delayed(Duration.zero).then((value) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      lastSync                = DateTime.parse(prefs.getString("last_sync") ?? DateTime.now().toString());
      log("last sync date $lastSync");
    });
  }

  static getLastLogin() async {
    Future.delayed(Duration.zero).then((value) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      if(prefs.getString("last_login") == null){
        lastLogin = null;
        log("last login date $lastLogin");
        return lastLogin;
      }
      else {

        lastLogin  = DateTime.parse(prefs.getString("last_login")!);
        log("last login date $lastLogin");
        return lastLogin;
      }

    });
  }


  static checkIfAfterLogin() async {
    Future.delayed(Duration.zero).then((value) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      isAfterLogin            = prefs.getBool(afterLoginKey) ?? false;
      log("is logged in $isAfterLogin");
    });
  }


  static checkMainMenu() async {
    Future.delayed(Duration.zero).then((value) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      bool?  resultHealthInfo            = prefs.getBool(FunctionShared.healthInfoKey);
      if(resultHealthInfo == null){
        healthInfo = false;
      }
      else {
        healthInfo = resultHealthInfo;
      }

      bool?  resultVehicleInfo            = prefs.getBool(FunctionShared.vehicleInfoKey);
      if(resultVehicleInfo == null){
        vehicleInfo = false;
      }
      else {
        vehicleInfo = resultVehicleInfo;
      }

      bool? resultP2hInfo            = prefs.getBool(FunctionShared.p2hInfoKey);
      if(resultP2hInfo == null){
         p2hInfo = false;
      }
      else {
        p2hInfo = resultP2hInfo;
      }

      bool? resultSitaInfo            = prefs.getBool(FunctionShared.sitaInfoKey);
      if(resultSitaInfo == null){
        sitaInfo = false;
      }
      else {
        sitaInfo = resultSitaInfo;
      }
    });
  }

  static isLoggedInApps() async {
    Future.delayed(Duration.zero).then((value) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      bool? isLoggedInToApps   = prefs.getBool(FunctionShared.isLoggedInKey) ?? false;
      log("is logged $isLoggedInToApps");
      prefs.reload();
      FunctionShared.isLoggedIn = isLoggedInToApps;
      //isLoggedInApps();
    });
  }

  static getLocationIndex() async {
    Future.delayed(Duration.zero).then((value) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      int? indexs   = prefs.getInt('index_location') ?? 0;
      prefs.reload();
      indexLocation = indexs;
      getLocationIndex();
    });
  }

  static setLocationSelected(int value) async {
    Future.delayed(Duration.zero).then((x) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setInt("index_location",value);
      indexLocation = value;
    });
  }

  static setLoggedInApps(bool value) async {
    Future.delayed(Duration.zero).then((x) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setBool(FunctionShared.isLoggedInKey,value);
      isLoggedIn = value;
      isLoggedInApps();
    });
  }

  static isModeUser() async {
    Future.delayed(Duration.zero).then((value) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      bool? isModeUsers   = prefs.getBool("login_mode") ?? false;
      log("is mode user $isModeUsers");
      prefs.reload();
      userMode = isModeUsers;
    });
  }

  static setModeLogin(bool value) async {
    Future.delayed(Duration.zero).then((x) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setBool("login_mode",value);
      userMode = value;
      isModeUser();
    });
  }

  static Future<void> initPlatformState() async {
    var deviceData = <String, dynamic>{};

    try {
      if (kIsWeb) {
        //deviceData = _readWebBrowserInfo(await deviceInfoPlugin.webBrowserInfo);
      } else {
        if (Platform.isAndroid) {
          deviceData = _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
          String deviceInfo = "${deviceData['brand']}::${deviceData['androidId']}";
          saveDeviceId(deviceInfo, deviceInfoKey);
        } else if (Platform.isIOS) {
          deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
          String deviceInfo = "${deviceData['name']}::${deviceData['identifierForVendor']}";
          saveDeviceId(deviceInfo, deviceInfoKey);
        } else if (Platform.isLinux) {
          //deviceData = _readLinuxDeviceInfo(await deviceInfoPlugin.linuxInfo);
        } else if (Platform.isMacOS) {
          //deviceData = _readMacOsDeviceInfo(await deviceInfoPlugin.macOsInfo);
        } else if (Platform.isWindows) {
          //deviceData = _readWindowsDeviceInfo(await deviceInfoPlugin.windowsInfo);
        }
      }
    } on PlatformException {
      deviceData = <String, dynamic>{
        'Error:': 'Failed to get platform version.'
      };
    }


  }

  static String formatDateDDMMMMYYYYHHMM({required DateTime date}){
    return DateFormat("dd MMMM yyyy HH:mm").format(date);
  }

  static String formatDateDDMMYYYYHHMM({required DateTime date}){
    return DateFormat("yyyy-MM-dd HH:mm:ss").format(date);
  }

  static String formatDateYYYYMMDDHHMMSS({required DateTime date}){
    return DateFormat("yyyy-MM-dd HH:mm:ss").format(date);
  }

  static Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'version.securityPatch': build.version.securityPatch,
      'version.sdkInt': build.version.sdkInt,
      'version.release': build.version.release,
      'version.previewSdkInt': build.version.previewSdkInt,
      'version.incremental': build.version.incremental,
      'version.codename': build.version.codename,
      'version.baseOS': build.version.baseOS,
      'board': build.board,
      'bootloader': build.bootloader,
      'brand': build.brand,
      'device': build.device,
      'display': build.display,
      'fingerprint': build.fingerprint,
      'hardware': build.hardware,
      'host': build.host,
      'id': build.id,
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
      'supported32BitAbis': build.supported32BitAbis,
      'supported64BitAbis': build.supported64BitAbis,
      'supportedAbis': build.supportedAbis,
      'tags': build.tags,
      'type': build.type,
      'isPhysicalDevice': build.isPhysicalDevice,
      'systemFeatures': build.systemFeatures,
      'androidId':build.androidId
    };
  }
  static Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'name': data.name,
      'systemName': data.systemName,
      'systemVersion': data.systemVersion,
      'model': data.model,
      'localizedModel': data.localizedModel,
      'identifierForVendor': data.identifierForVendor,
      'isPhysicalDevice': data.isPhysicalDevice,
      'utsname.sysname:': data.utsname.sysname,
      'utsname.nodename:': data.utsname.nodename,
      'utsname.release:': data.utsname.release,
      'utsname.version:': data.utsname.version,
      'utsname.machine:': data.utsname.machine,
    };
  }
  static Future<Position> determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition();
  }
  // ignore: non_constant_identifier_names
  static Future<void> openMap(BuildContext context, double lat, double lng,String place_id) async {
    String urlAppleMaps = '';
    if (Platform.isAndroid) {
      urlAppleMaps = 'https://www.google.com/maps/search/?api=1&query=$lat,$lng&query_place_id=$place_id';
    } else {
      urlAppleMaps = "https://www.google.com/maps/search/?api=1&query=$lat,$lng&query_place_id=$place_id";
      // url = 'https://maps.googleapis.com/maps/api/place/details/json?place_id=$place_id&key=AIzaSyD_uTvA6CvfNELsAH8YTIMDzYSdP6ul6Ss';
      if (await canLaunchUrlString(urlAppleMaps)) {
        await launchUrlString(urlAppleMaps);
      } else {
        throw 'Could not launch $urlAppleMaps';
      }
    }
  }
  static openWa({required String phoneNumber,required String name}) async {
    var whatsappUrl = "https://api.whatsapp.com/send?phone=$phoneNumber&text=Hi, $name&source=&data=";

    if (await canLaunchUrlString(whatsappUrl)) {
    await launchUrlString(whatsappUrl);
    } else {
    throw 'Could not launch $whatsappUrl';
    }
  }

  static openURL({required String url}) async {

    if (await canLaunchUrl(Uri.parse(url))) {
      //await launchUrl(url, mode: LaunchMode.externalApplication);
      await launchUrl(Uri.parse(url),mode: LaunchMode.externalApplication);
    } else {
      throw 'Could not launch $url';
    }
  }

  static removeAll() async {
    Future.delayed(Duration.zero).then((value) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.clear();
    });
  }

  static String numberFormatIDR({dynamic price}){
    return price.toString().contains("-") ?
    "IDR ${NumberFormat.currency(locale: 'id',decimalDigits: 0, customPattern: '#,###').format(price).toUpperCase()}" :
     NumberFormat.currency(locale: 'id',decimalDigits: 0, customPattern: 'IDR #,###').format(price).toUpperCase();
  }
  static String numberFormatRpEN({dynamic price}){
    if(double.tryParse(price.toString()) == null){
      return price;
    } else {
      return NumberFormat.currency(locale: 'en',decimalDigits: 0, customPattern: 'Rp #,###').format(price);
    }
  }
  static String numberFormatRpID({dynamic price}){
    if(double.tryParse(price.toString()) == null){
      return price;
    } else {
      return NumberFormat.currency(locale: 'id',decimalDigits: 0, customPattern: 'Rp #,###').format(price is String?double.tryParse(price): price);
    }
  }
  static String numberFormatIDRNoSymbol({required dynamic price}) {
    return NumberFormat.currency(locale: 'id', decimalDigits: 0, customPattern: '#,###').format(price).toUpperCase();
  }
  static String numberFormatNoSymbol({required dynamic price}) {
    return NumberFormat.currency(locale: 'id', decimalDigits: 0, customPattern: '#,###').format(num.parse(price)).toUpperCase();
  }

  static String formatDateDDMMMMYYYY({required DateTime date}){
    return DateFormat("dd MMMM yyyy").format(date);
  }
  static String formatDateYYYYMMDD({required DateTime date}){
    return DateFormat("yyyy-MM-dd").format(date);
  }

  static checkRoleMenu({dynamic menuName,dynamic functionName}){
    List dataTrue = [];
    if(enableRole == true){
      if(profile!['data']['user_data'].any((e) => e['function_name'].toString().toLowerCase() == functionName.toString().toLowerCase() && e['menu_name'].toString().toLowerCase().replaceAll("_", " ") == menuName.toString().toLowerCase())){
        profile!['data']['user_data'].map((E){
          if(E['function_name'].toString().toLowerCase() == functionName.toString().toLowerCase() && E['menu_name'].toString().toLowerCase().replaceAll("_", " ") == menuName.toString().toLowerCase()){
            if(E['active']){
              dataTrue.add(E);
            }
          }
        }).toList();

        if(dataTrue.isNotEmpty){
          return dataTrue.first['active'];
        }
        else {
          return false;
        }
      }
      else {
        return false;
      }
    }
    else{
      return true;
    }
  }

  //GMT
  static String setTimeWithGMT({required String dateTimeString}) {
    Duration duration = DateTime.now().timeZoneOffset;
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoHour          = twoDigits(duration.inHours);
    String twoDigitMinutes  = twoDigits(duration.inMinutes.remainder(60));
    if(duration.inMinutes > 0 ){
      DateTime dateTime = DateTime.parse(dateTimeString).add(duration);
      var gmt = "GMT+$twoHour:$twoDigitMinutes";
      log("datetime + GMT $dateTime");
      log("timezone offset $gmt");
      return dateTime.toString();
    }
    else {
      DateTime dateTime = DateTime.parse(dateTimeString).subtract(duration);
      var gmt = "GMT-$twoHour:$twoDigitMinutes";
      log("datetime - GMT $dateTime");
      log("timezone offset $gmt");
      return dateTime.toString();
    }
  }

  //GMT
  static String setTimeWithGMTFormatMMDDYYYYHHMM({required String dateTimeString}) {
    if(dateTimeString.isNotEmpty){
      Duration duration       = DateTime.now().timeZoneOffset;
      String twoDigits(int n) => n.toString().padLeft(2, "0");
      String twoHour          = twoDigits(duration.inHours);
      String twoDigitMinutes  = twoDigits(duration.inMinutes.remainder(60));
      if(duration.inMinutes > 0 ){
        DateTime dateTime = DateTime.parse(dateTimeString).add(duration);
        var gmt = "GMT+$twoHour:$twoDigitMinutes";
        log("datetime + GMT $dateTime");
        log("timezone offset $gmt");
        return DateFormat("dd MMMM yyyy HH:mm").format(dateTime);
      }
      else {
        DateTime dateTime = DateTime.parse(dateTimeString).add(duration);
        var gmt = "GMT-$twoHour:$twoDigitMinutes";
        log("datetime - GMT $dateTime");
        log("timezone offset $gmt");
        return DateFormat("dd MMMM yyyy HH:mm").format(dateTime);
      }
    }
    else {
      return "-";
    }

  }

  static setColorStatusSpk({dynamic status}){
    switch(status){
      case "Accepted"   : return HexColor("#E5C100");
      case "Completed"  : return HexColor("#28B837");
      case "Released"   : return HexColor("#28B837");
      case "Open"       : return HexColor("#286BBA");
      case "Canceled"   : return HexColor("#999999");
      default           : return HexColor("#999999");
    }
  }

  static int daysBetween(DateTime from, DateTime to) {
    from = DateTime(from.year, from.month, from.day, from.hour,from.minute);
    to = DateTime(to.year, to.month, to.day,to.hour, to.minute);
    log("from $from");
    log("to $to");
    log("different ${(to.difference(from).inSeconds / 60).round()}");
    return (to.difference(from).inSeconds / 60).floor();
  }
}



extension StringExtension on String {
  // String capitalize() {
  //   if(isNotEmpty) {
  //     return "${this[0].toUpperCase()}${substring(1).toLowerCase()}";
  //   }
  //   return "";
  // }

  String toCapitalized() => length > 0 ?'${this[0].toUpperCase()}${substring(1).toLowerCase()}':'';
  String capitalize() =>  toUpperCase();
}

extension GenerateNumber on String {

  String generateNumber(){
    if(length == 1){
      return "000$this";
    }
    else if(length == 2){
       return "00$this";
    }
    else if(length == 3){
      return "0$this";
    }
    else if(length == 4){
      return this;
    }
    else{
     return this;
    }
  }
}