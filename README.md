# siswaapp

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


## Dokumentasi aplikasi :

1. Login user :
   - username : admin
   - password : admin123

2. Aplikasi menggunakan database sqlite/sqflite untuk flutter
3. untuk setiap action saya berikan delay 2 detik agar terkesan behaviour nya terasa, 
   karen kalau tidak diberikan delay, akan sangat cepat dalam melakakuan peengambilan data maupun insert data. 
   contohnya loading pada saat login dan get list data siswa.
4. Untuk membantu saya dalam develop, saya menggunakan beberapa hal :
   - Flutter Modular untuk menjaga cycle aplikasi tetap stabil
   - state managemen yg saya gunakan adalah Provider
   - dan beberpa library yang terdapat pada pubspec.yaml.

note : Dokumentasi ini saya tulis setelah subuh, dikarenakan saya tidak jadi bisa hadir 
    karena motor saya masih mogok sampe dokumntasi ini dibuat.


Mohon maaf atas ketidak hadirannya, dan terima kasih,


-Abu Khoerul Iskandar Ali-